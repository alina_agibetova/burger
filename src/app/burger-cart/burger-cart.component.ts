import {Component, Input} from '@angular/core';
import {Burger} from "../shared/ingredient";

@Component({
  selector: 'app-burger-cart',
  templateUrl: './burger-cart.component.html',
  styleUrls: ['./burger-cart.component.css']
})
export class BurgerCartComponent {
  @Input() burgers!: Burger[];
  @Input() ingredients!: Burger[];



}
