import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Burger} from "../../shared/ingredient";


@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent {
  @Output() newBurger = new EventEmitter<Burger>();
  @Input() burgers!: Burger[];
  @Input() burger!: Burger;


  onAddIngredients(event:Event) {
    event.preventDefault();
    const target = <HTMLImageElement>event.target;
    console.log(target.id);
    this.burger.number++;
  }

  // deleteIngredient(i: number) {
  //   this.burgers.splice(i);
  // }

  createBurger() {
    const burger = new Burger('meat', 'cheese', 'salad', 'bacon', 1, 20);
    this.newBurger.emit(burger);
  }


}
