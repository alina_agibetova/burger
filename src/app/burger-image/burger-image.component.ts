import {Component, Input} from '@angular/core';
import {Burger} from "../shared/ingredient";

@Component({
  selector: 'app-burger-image',
  templateUrl: './burger-image.component.html',
  styleUrls: ['./burger-image.component.css']
})
export class BurgerImageComponent {
  @Input() burgers!: Burger[];
  @Input() ingredients!: Burger[];


  ingredient = new Burger('meat', 'cheese', 'salad', 'bacon', 1, 20).getPrice();
}
