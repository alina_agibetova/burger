export class Burger {
  constructor(
    public nameMeat: string,
    public nameCheese: string,
    public nameSalad: string,
    public nameBacon: string,
    public number: number,
    public price: number,
  ){}



  getPrice(){
    const total = (this.number * this.price) * 20;
    return total;
  }

}


