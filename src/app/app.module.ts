import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BurgerImageComponent } from './burger-image/burger-image.component';
import { BurgerCartComponent } from './burger-cart/burger-cart.component';
import { ItemComponent } from './burger-cart/item/item.component';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    BurgerImageComponent,
    BurgerCartComponent,
    ItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
