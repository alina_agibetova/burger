import { Component } from '@angular/core';
import {Burger} from "./shared/ingredient";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  ingredients = new Burger('meat', 'cheese', 'salad', 'bacon', 1, 20).getPrice();

  burgers: Burger[] = [
    new Burger('meat', 'cheese', 'salad', 'bacon', 1, 20),
  ];

  constructor(){
    console.log(this.ingredients);
  }


  onNewBurger(burger: Burger){
    this.burgers.push(burger);
  }
}
